# Consignes pour jouer au jeu.

## Prérequis

1. Un système Linux ou un compilateur C (comme GCC)
2. Les fichiers suivants : 
    - stack.c
    - stack.h
    - matrix.c
    - matrix.h
    - game.c
    - Makefile

## Installation

+ Ouvrez votre terminal Linux
+ Entrez dans le dossier où vous avez décompressez les fichiers cités plus haut
+ Tapez la commande ``` make jeu ``` dans le terminal
+ Tapez la commande ``` ./jeu ```
+ Enjoy !

## Principe du jeu

C'est un jeu qui se joue avec plusieurs joueurs (le nombre de votre choix) et sur un plateau de la taille de votre choix. 

Le jeu est composé d'un plateau composé lui même de cases. Le jeu se joue au tour par tour. 

Chaque joueur, lorsque c'est à leur tour, doit déposer un jeton dans la case de son choix qui ira s'emmpiler avec d'éventuel jeton
déjà présent. Lorsque le joueur arrive à avoir deux jetons de sa couleur d'affilé dans une même pile, celle-ci s'active, retire
les jetons en double de la pile et dépose un jeton de la couleur du joueur dans les cases voisines qui peuvent, s'il ne l'ont pas été, s'activer
à leur tour. 

La partie se termine lorsque le plateau est rempli, c'est-à-dire lorsque toute les cases ont au moins un jeton dans leur pile. 

Celui qui a le plus de jeton au dessus d'une pile gagne. 

## Comment jouer

* Donnez le nombre de joueurs
* Donnez la taille du plateau
* Entrez les coordonnées de la case voulez sous la forme **i j** où i est la ligne de la case et j la colonne
* validez en entrant la lettre 'o' ou changez de pile en entrant la lettre 'n'
