#ifndef _STACK_H
#define _STACK_H

typedef int value;
typedef struct node *stack;

struct node {
    value val;
    stack next;
};

stack stackCreate();

int stackEmpty(stack p);

void stackPush(stack *p, value v);

value stackPop(stack *p);

void stackPrint(stack p);

void stackFree(stack *p);

#endif