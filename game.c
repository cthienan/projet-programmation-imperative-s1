#include <stdio.h>
#include <stdlib.h>
#include "matrix.h"
#include "stack.h"

/*@requires : p has valid adress
  @assigns : nothing
  @ensures : test if the two last values added to p are equals */
int testActivate(stack p) {
    // on teste si la pile a au moins deux jetons
    if (p->next != NULL) {
        value v1 = p->val;
        value v2 = (p->next)->val;
        return (v1 == v2); // on teste si les deux jetons en haut de la pile sont égaux
    }
    else {
        //la pile a moins de deux jetons
        return 0;
    }
}

/*@requires : n is the length of M and ligne and colonne have valid adress
  @assigns : ligne and colonne are modified
  @ensures : push the value v in the stack M[i][j] and add its coordinates in ligne and colonne*/
void activateCell(matrix M, int i, int j, value v, stack *ligne, stack *colonne, int **activated) {
    // on regarde si la case a déjà été activée
    if (activated[i][j] != 1) {
        stackPush(&M[i][j], v); // on ajoute un jeton dans cette cases
        stackPush(ligne, i); stackPush(colonne, j); // on enregistre ses cordonnées ppur voir ensuite si cette case peut être activée
    }
}

/*@requires : n is the length of M and ligne and colonne have valid adress
  @assigns : ligne, colonne and activated are modified
  @ensures : if testActivate is true then it will push the common value in all the surrounding stacks and add 1 in the table activated
            to say that this stack has been already activated */
void activate(matrix M, stack *ligne, stack *colonne, int n, int **activated){
    while(*ligne != NULL) {
    /*Ici, il existe qu'un nombre fini de cases qui peuvent être activée car elles ne peuvent pas être activé plus d'une fois, 
    donc ligne sera nécessairement de taille fini, et puisqu'à chaque itération, on retire un élément, au bout d'un certain temps, ligne sera vide */
        int i = stackPop(ligne);
        int j = stackPop(colonne);

        //on teste s'il y a deux jetons dans la pile et si oui, que les deux jetons au-dessus de la pile sont égaux
        if (testActivate(M[i][j])) {

            // on retire les deux jetons égaux de la pile
            value v = stackPop(&(M[i][j]));
            stackPop(&(M[i][j]));

            //on regarde si la case est celle en haut à gauche
            if (i == 0 && j == 0) {
                activateCell(M, i, j+1, v, ligne, colonne, activated);
                activateCell(M, i+1, j, v, ligne, colonne, activated);
            }

            //on regarde si la case est sur le bord du haut
            else if (i == 0 && j != 0 && j != n-1) {
                activateCell(M, i, j+1, v, ligne, colonne, activated);
                activateCell(M, i+1, j, v, ligne, colonne, activated);
                activateCell(M, i, j-1, v, ligne, colonne, activated);
            }

            //on regarde si la case est celle en haut à droite
            else if (i == 0 && j == n-1) {
                activateCell(M, i+1, j, v, ligne, colonne, activated);
                activateCell(M, i, j-1, v, ligne, colonne, activated);
            }

            //on regarde si la case est sur le bord droit
            else if (j == n-1 && i != 0 && i != n-1) {
                activateCell(M, i-1, j, v, ligne, colonne, activated);
                activateCell(M, i+1, j, v, ligne, colonne, activated);
                activateCell(M, i, j-1, v, ligne, colonne, activated);
            }

            //on regarde si la case est celle en bas à droite
            else if (j == n-1 && i == n-1) {
                activateCell(M, i-1, j, v, ligne, colonne, activated);
                activateCell(M, i, j-1, v, ligne, colonne, activated);
            }

            //on regarde si la case est sur le bord du bas
            else if (i == n-1 && j != 0 && j != n-1) {
                activateCell(M, i-1, j, v, ligne, colonne, activated);
                activateCell(M, i, j+1, v, ligne, colonne, activated);
                activateCell(M, i, j-1, v, ligne, colonne, activated);
            }

            //on regarde si la case est celle en bas à gauche
            else if (i == n-1 && j == 0) {
                activateCell(M, i-1, j, v, ligne, colonne, activated);
                activateCell(M, i, j+1, v, ligne, colonne, activated);
            }

            //on regarde si la case est sur bord gauche
            else if (j == 0 && i != 0 && i != n-1) {
                activateCell(M, i-1, j, v, ligne, colonne, activated);
                activateCell(M, i, j+1, v, ligne, colonne, activated);
                activateCell(M, i+1, j, v, ligne, colonne, activated);
            }

            //la case n'est pas sur les bords
            else {
                activateCell(M, i-1, j, v, ligne, colonne, activated);
                activateCell(M, i, j+1, v, ligne, colonne, activated);
                activateCell(M, i+1, j, v, ligne, colonne, activated);
                activateCell(M, i, j-1, v, ligne, colonne, activated);
            }
            activated[i][j] = 1; //on indique que la case (i,j) a déjà été activée
        }
    }
}

/*@requires : n > 0
  @assigns : nothing
  @ensures : return the maximum of t */
int maxTab (int* t, int n) {
    int max = t[0];
    int m = 0;
    for (int i = 0 ; i < n ; i++) {
        if (t[i] > max) {
            max = t[i];
            m = i;
        }
    }
    return m;
}

/*@requires : n is the length of M
  @assigns : nothing
  @ensures : return the player who has the most token on the top of the board*/
int winner (matrix M, int n, int nb_joueurs) {
    int *tab = calloc(nb_joueurs+1, sizeof(int));

    for (int i = 0 ; i < n ; i++) {
        for (int j = 0 ; j < n ; j++) {
            value k = stackPop(&M[i][j]);
            tab[k] += 1;
        }
    }
    int m = maxTab(tab, nb_joueurs+1);
    free(tab);

    return m;
}

int main() {

    char buf[256];
    int nb_joueurs = 0;

    // on demande le nombre de joueurs en faisant attention que l'entrée soit valide
    printf("Nombre de joueurs : \n");
    while (nb_joueurs <= 0) {
    /*la fin de la boucle dépend de l'utilisateur*/
        fgets(buf, 256, stdin);
        sscanf(buf, "%d", &nb_joueurs);
        if (nb_joueurs <= 0)
            printf("Cet entrée est invalide, Veuillez recommencer.\n");
    }

    int taille = 0;

    // on demande la taille du plateau en faisant attention que l'entrée soit valide
    printf("taille du plateau : \n");
    while (taille <= 0) {
    /*la fin de la boucle dépend de l'utilisateur*/
        fgets(buf, 256, stdin);
        sscanf(buf, "%d", &taille);
        if (taille <= 0)
            printf("Cet entrée est invalide, Veuillez recommencer.\n");
    }

    // on crée le plateau
    matrix plateau = matrixCreate(taille);

    int ligne = 0;
    int colonne = 0;
    int joueur = 1;
    int cond = 0;
    char *accept = calloc(256,sizeof(char)); //la variable de confirmation

    while (cond == 0) {
    /*Cette boucle ne finit que lorsque le plateau sera rempli, donc si les joueurs jouent sérieusement*/

        printf("Tour du joueur %d.\n", joueur);
        printf("\n");
        
        //on affiche le plteau
        matrixPrint(plateau, taille);
        printf("\n");

        while(accept[0] != 'o') {
        /*La boucle ne se finit que lorsque l'utilisateur entre un mot commençant par la lettre 'o'*/

            //on demande la case au joueur
            fgets(buf, 256, stdin);
            sscanf(buf, "%d %d", &ligne, &colonne);
            
            //on affiche l'état de la pile choisie par le joueur
            stackPrint(plateau[ligne-1][colonne-1]);
            printf("\n");

            //on demande confirmation au joueur
            printf("Voulez-vous mettre le jeton ici ?\n");

            fgets(buf, 256, stdin);
            sscanf(buf, "%s", accept);
        }

        //on ajoute le jeton du joueur à la pile
        stackPush(&plateau[ligne-1][colonne-1], joueur);

        //on génère les pile qui vont contenir les coordonnées des cases à activer
        stack row = stackCreate();
        stackPush(&row, ligne-1);
        stack col = stackCreate();
        stackPush(&col, colonne-1);

        //on génère la matrice qui indique si oui ou non une case a déjà été activée
        int **activated = malloc(taille * sizeof(int*));
        for (int k = 0 ; k < taille ; k++) {
            activated[k] = calloc(taille, sizeof(int));
        }

        //on teste les activations
        activate(plateau, &row, &col, taille, activated);

        //on libère l'esapce mémoire alloué à activated
        for (int k = 0 ; k < taille ; k++) {
            free(activated[k]);
        }
        free(activated);

        //on teste si la matrice est pleine
        if (matrixFull(plateau, taille))
            // si oui, on sort de la boucle
            cond = 1;
        else {
            //sinon, on passe au joueur suivant et on réinitialise la variable de confirmation
            joueur = (joueur % nb_joueurs) + 1;
            accept[0] = 'n';
        }
    }

    //on affiche l'état final du plateau
    matrixPrint(plateau, taille);
    printf("\n");
    printf("Le gagnant est le joueur %d\n", winner(plateau, taille, nb_joueurs));
    

    //on libère l'espace mémoire
    matrixFree(plateau, taille);
    free(accept);

    return 0;
}