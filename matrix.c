#include <stdio.h>
#include <stdlib.h>
#include "matrix.h"
#include "stack.h"

/*@requires : n > 0
  @assigns : nothing
  @ensures : create a matrix */
matrix matrixCreate(int n) {
    matrix M = malloc(n * sizeof(stack*));

    for (int i = 0 ; i < n ; i++) {
        M[i] = malloc(n * sizeof(stack));
    }

    for (int i = 0 ; i < n ; i++) {
        for (int j = 0 ; j < n ; j++) {
            M[i][j] = stackCreate();
        }
    }

    return M;
}

/*@requires : n is the length of M
  @assigns : nothing
  @ensures : test if the matrix full, that is to say that all the stack have at least one value in it */
int matrixFull(matrix M, int n){
    int compt = 1;

    for (int i = 0 ; i < n ; i++) {
        for (int j = 0 ; j < n ; j++) {
            if (stackEmpty(M[i][j])) {
                compt = 0;
            }
        }
    }
    return compt;
}

/*@requires : n is the length of M
  @assigns : nothing
  @ensures : print the matrix */
void matrixPrint(matrix M, int n){
    printf(" |");
    for (int i = 1 ; i <= n ; i++)
        printf("%2d", i);
    printf("\n");
    printf("-+");

    for (int i = 0 ; i < n ; i++)
        printf("--");
    printf("-\n");

    for (int i = 0 ; i < n ; i++) {
        printf("%d|", i+1);

        for (int j = 0 ; j < n ; j++) {
            if(M[i][j] == NULL)
                printf("  ");
            else
                printf("%2d", M[i][j]->val);
        }
        printf("\n");
    }
}

/*@requires : n is the length of M
  @assigns : nothing
  @ensures : free the memory allocated to the matrix */
void matrixFree(matrix M, int n) {
    for (int i = 0 ; i < n ; i++) {
        for (int j = 0 ; j < n ; j++) {
            stackFree(&M[i][j]);
        }
        free(M[i]);
    }
    free(M);
}