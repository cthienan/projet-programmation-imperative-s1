#ifndef _MATRIX_H
#define _MATRIX_H

#include "stack.h"

typedef stack** matrix;

matrix matrixCreate(int n);

int matrixFull(matrix M, int n);

void matrixPrint(matrix M, int n);

void matrixFree(matrix M, int n);

#endif