#include <stdio.h>
#include <stdlib.h>
#include "stack.h"

/*@requires : nothing
  @assigns : nothing
  @ensures : create a stack */
stack stackCreate(){
    stack p; 
    p = NULL;
    return p;
}

/*@requires : p has valid adress
  @assigns : nothing
  @ensures : test if p is empty */
int stackEmpty(stack p){
    return (p == NULL);
}

/*@requires : p has valid adress
  @assigns : modify p
  @ensures : add v in the stack p */
void stackPush(stack *p, value v){
    stack res = malloc(sizeof(struct node));
    res->val = v;
    res->next = *p;
    *p = res;
}

/*@requires : p has valid adress
  @assigns : modify p
  @ensures : remove and print the last value added in p */
value stackPop(stack *p){
    if (*p != NULL) {
        stack tmp = (*p)->next;
        int v = (*p)->val;
        free(*p);
        *p = tmp;
        return v;
    }
    else {
        printf("Erreur : Pile déjà vide.\n");
        exit(1);
    }
}

/*@requires : p has valid adress
  @assigns : nothing
  @ensures : print the stack */
void stackPrint(stack p) {
    while (p != NULL) {
    /* p est de taille fini et l'instruction p = p->next diminue la taille de p
    donc p sera nécessairement égal à NULL au bout d'un certain temps */
        printf("%d\n", p->val);
        p = p->next;
    }
    printf("---\n");
}

/*@requires : p has valid adress
  @assigns : modify p
  @ensures : free the memory allocated to p */
void stackFree(stack *p) {
    while (*p != NULL) {
    /* p est de taille fini et stackPop retire une valeur à chaque itération, 
    donc p sera nécessairement vide au bout d'un certain temps */
        stackPop(p);
    }
    free(*p);
}
