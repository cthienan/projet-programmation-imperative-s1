jeu : stack.o matrix.o game.o
	gcc -Wextra -Wall -o jeu stack.o matrix.o game.o

stack.o : stack.c
	gcc -Wextra -Wall -o stack.o -c stack.c

matrix.o : matrix.c stack.h
	gcc -Wextra -Wall -o matrix.o -c matrix.c

game.o : game.c stack.h matrix.h
	gcc -Wextra -Wall -o game.o -c game.c
